//
//  ViewController.swift
//  BlueTooth_Test_01
//
//  Created by fred fu on 2020/8/19.
//  Copyright © 2020 fred fu. All rights reserved.
//

import UIKit
import CoreBluetooth


class ViewContorller: UIViewController {
    
    
    private let Service_UUID: String = "CDD1"
    private let Characteristic_UUID: String = "CDD2"
    
    @IBOutlet weak var textField: UITextField!
    private var centralManager: CBCentralManager?
    private var peripheral: CBPeripheral?
    private var characteristic: CBCharacteristic?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "藍牙設備中心端"
        centralManager = CBCentralManager.init(delegate: self, queue: .main)
    
    }
    
    @IBAction func didClickGet(_ sender: Any) {
        self.peripheral?.readValue(for: self.characteristic!)
    }
    
    @IBAction func didClickPost(_ sender: Any) {
        let data = (self.textField.text ?? "empty input")!.data(using: String.Encoding.utf8)
        self.peripheral?.writeValue(data!, for: self.characteristic!, type: CBCharacteristicWriteType.withResponse)
    }
    
}

extension ViewContorller: CBCentralManagerDelegate, CBPeripheralDelegate {
    

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("未知的")
        case .resetting:
            print("重置中")
        case .unsupported:
            print("不支持")
        case .unauthorized:
            print("未驗證")
        case .poweredOff:
            print("未啟動")
        case .poweredOn:
            print("可用")
            central.scanForPeripherals(withServices: [CBUUID.init(string: Service_UUID)], options: nil)
        }
    }


    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        self.peripheral = peripheral

//        if (peripheral.name?.hasPrefix("WH"))! {
//            central.connect(peripheral, options: nil)
//        }
        central.connect(peripheral, options: nil)
    }
    

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        self.centralManager?.stopScan()
        peripheral.delegate = self
        peripheral.discoverServices([CBUUID.init(string: Service_UUID)])
        print("連接成功")
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
         print("連接失敗")
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("斷開連接")

        central.connect(peripheral, options: nil)
    }
    

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service: CBService in peripheral.services! {
            print("外設中的服務有：\(service)")
        }

        let service = peripheral.services?.last

        peripheral.discoverCharacteristics([CBUUID.init(string: Characteristic_UUID)], for: service!)
    }
    

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic: CBCharacteristic in service.characteristics! {
            print("外設中的特徵有：\(characteristic)")
        }
        
        self.characteristic = service.characteristics?.last

        peripheral.readValue(for: self.characteristic!)

        peripheral.setNotifyValue(true, for: self.characteristic!)
    }
    

    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            print("訂閱失敗: \(error)")
            return
        }
        if characteristic.isNotifying {
            print("訂閱成功")
        } else {
            print("取消訂閱")
        }
    }
    

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        let data = characteristic.value
        self.textField.text = String.init(data: data!, encoding: String.Encoding.utf8)
    }
    

    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("寫入資料")
    }
}
